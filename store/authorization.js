import Cookie from 'cookie'
import Cookies from 'js-cookie'

export const state = () => ({
    token: null
})

export const mutations = {
    setToken(state, token) {
        state.token = token;
    },
    clearToken(state) {
        state.token = null;
    }
}

export const actions = {
    async login({commit, dispatch}, formData) {
        try {
            // Call authorization here
            
            const token = 'TOKEN'
            await new Promise((resolve, reject) => {
                setTimeout(() => resolve('Error'), 1000)
            })

            dispatch('setToken', token);
        }
        catch(e) {
            commit('setError', e, { root: true })
            throw e;
        }
    },
    setToken({commit}, token) {
        commit('setToken', token)
        Cookies.set('api-token', token)
    },    
    logout({commit}) {
        commit('clearToken')
        Cookies.remove('api-token')
    },  
    autoLogin({ dispatch }) {
        const cookieStr = process.browser ? document.cookie : this.app.context.req.headers.cookie;
        const cookies = Cookie.parse(cookieStr || '') || {};
        const token = cookies['api-token'];

        // Check token on server here

        if (token == 'TOKEN') {
            dispatch('setToken', token)
        } 
        else {
            dispatch('logout')
        }
    }      
}

export const getters = {
    isAuthorized: state => Boolean(state.token),
    token: state => state.token,
}