export default function({store, redirect}) {
    if(store.getters['authorization/isAuthorized']) {
        redirect('/')
    }
}